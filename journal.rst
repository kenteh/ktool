.. 20220225

Finally! I have the beginnings of a proper version controlled ktool.

Ran into a problem using ktool with cmake's FetchContent().  Starting again,
this time using modern cmake's recommended project layout.

.. 20220401

New layout per "Modern CMake for C++".
::

  ktool
  ├── CMakeLists.txt
  ├── include/
  │   └── ktool/
  │       ├── kthread.h
  │       ├── sigpair.h
  │       ├── stringutils.h
  │       └── version.h.in
  ├── journal.rst
  ├── README.rst
  ├── sigpair.c
  ├── stringutils.cxx
  └── support/
      ├── ktool-config.cmake.in
      └── ktool.pc.in

  3 directories, 11 files

A working CMakeLists.txt that installs together with a ktool-config.cmake to
support find_package() and a ktool.pc to support pkg-config.

Next steps:

- test importing with find_package(); done

- test fetchcontent; requires push to gitlab.

Converted into a public repo on gitlab.


.. 20220403

**Reminder** There are a couple of .py files in lib/python3.  Consider making
them part of ktool.

To test fetchcontent, we need to store ktool into a public git repo. 

.. 20220505

Encountered the following problem when linking to ktool. ::

  ld: warning: object file (/Users/teh/lib/libktool.a(sigpair.c.o)) was built for
   newer macOS version (12.1) than being linked (11.0)

The fix is to set add the following to CXXFLAGS ::

  -mmacosx-version-min=12.1

CMake sets this compiler flag from the OSX_DEPLOYMENT_TARGET cache variable if
it exists. Otherwise it computes it from CMAKE_OSX_SYSROOT. If necessary, add
the following lines to cmakelists. ::

  if(${APPLE})
    set(CMAKE_OSX_DEPLOYMENT_TARGET 12.1 CACHE STRING 
    "OSX Deployment Target")
  endif()


.. 20220625

Solved the 'object file ... was built for newer macOS ... than being linked ...'
problem.  Basically, we need the following linking command
::

  $(CXX) -mmacosx-version-min=12.1 $^ -o $@ $(RPATHLIBS) -lktool

The min version should be compatible with what "deployment target" was specified
when building the ktool library on OSX.  

Have updated ktool's cmakelists to set CMAKE_OSX_DEPLOYMENT_TARGET. ::

  teh@Kens-Mac-mini osxlib % head -10 CMakeLists.txt
  cmake_minimum_required(VERSION 3.20)

  set(CMAKE_BUILD_TYPE Release CACHE STRING "Release Build Type")
  set(CMAKE_OSX_DEPLOYMENT_TARGET "12.1" CACHE STRING 
      "Set the minimum macOSX SDK version")
  #set(CMAKE_OSX_SYSROOT 
  #    /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk)
  project(ktool
    VERSION 2.2
    LANGUAGES C CXX)
  teh@Kens-Mac-mini osxlib %  

The deployment target is the version of the selected SDK.  If it unspecified,
cmake uses CMAKE_OSX_SYSROOT to determine what it is. 

Use xcrun to determine CMAKE_OSX_SYSROOT.
::

  teh@Kens-Mac-mini osxlib % xcrun --show-sdk-path
  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk

There is a second form that lets you specify the specific SDK. Useful if you
need the SDK for iOS.
::

  teh@Kens-Mac-mini osxlib % xcrun --sdk macosx --show-sdk-path
  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk
  teh@Kens-Mac-mini osxlib %

If we look at the SDK's parent folder, we see
::

  teh@Kens-Mac-mini osxlib % tree -F -L 1 $SDK/..
  /Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/../
  ├── MacOSX.sdk -> MacOSX12.3.sdk/
  ├── MacOSX11.1.sdk/
  ├── MacOSX11.3.sdk/
  ├── MacOSX11.sdk -> MacOSX11.3.sdk/
  ├── MacOSX12.3.sdk/
  └── MacOSX12.sdk -> MacOSX12.3.sdk/

  6 directories, 0 files
  teh@Kens-Mac-mini osxlib %

there are several SDK versions with 12.3 being the current version. **NOTE**
that it is preferable to set the deployment target when building ktool since
using the osx sysroot uses the default SDK which is why the cmakelists has the
osx sysroot setting commented out.  **IMPORTANT** this variable must be set
before any cmake project() command.

The ktool's SDK version is 12.1 because that's the version on my macbook; it
cannot be upgraded to 12.3 which is on the mac mini.

Confirmed it works with the following test procedure.

- First build ktool and install into some temporary location, eg, an install
  folder inside cmake's source directory.

- Create a test program and a makefile based on simple.mk with the following
  changes. 
  ::

    teh@Kens-Mac-mini tests % diff ~/hackware/simple.mk Makefile
    3,6c3,6
    < IDIRS =
    < ifneq ($(wildcard $(HOME)/include),)
    <   IDIRS += -I$(HOME)/include
    < endif
    ---
    > IDIRS = -I../install/include
    > #ifneq ($(wildcard $(HOME)/include),)
    > #  IDIRS += -I$(HOME)/include
    > #endif
    22c22,23
    <     RPATHLIBS = -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib
    ---
    > #    RPATHLIBS = -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib
    >     RPATHLIBS = -Wl,-rpath,../install/lib -L../install/lib
    50c51
    < 	$(CXX) $^ -o $@ $(RPATHLIBS) -lfmt
    ---
    > 	$(CXX) -mmacosx-version-min=12.1 $^ -o $@ $(RPATHLIBS) -lktool
    teh@Kens-Mac-mini tests %

  ie, we build the test program against the installed ktool library and include
  the min macosx version when linking it.


.. 20231015

Added daemonize.c but removed it.  The daemonize() function is not necessary
anymore neither with linux or macos.  Use systemd or launchd to turn a program
into a daemon.

If a daemon needs to be coded, it is possible to use daemon().  But **note**
while it is deprecrated on macos (it generates warnings), it still works. It is
not deprecated on linux.  See hwt/daemonize for more details.

.. 20240714

Added lstrip and rstrip to stringutils. Updated CMakeLists.txt to require cmake
3.21 and minimum mac SDK to 14.2.
