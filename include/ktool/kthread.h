/**
 * @author Ken Teh
 */
#pragma once
#include <string>
#include <thread>
#include <functional>

namespace ktool {

template <class T>
class kthread {
    public:
        explicit kthread(const std::string &nm_)
            : nm(nm_), stop_requested(true), exited(true), pthrd(nullptr) {}
        kthread(kthread &&r) 
                : nm(r.nm), stop_requested(r.stop_requested),
                exited(r.exited), pthrd(r.pthrd) {
            r.pthrd = nullptr;
        }
        ~kthread() { if (pthrd != nullptr) stopjoin(); }

        void threadfn() { stop_requested = exited = true; }

        void start() {
            if (pthrd != nullptr) return;
            stop_requested = exited = false;
            pthrd = new std::thread(std::bind(&T::threadfn,
                        static_cast<T*>(this)));
        }
        void stopjoin() {
            if (pthrd == nullptr) return;
            stop_requested = true;
            pthrd->join();
            delete pthrd;
            pthrd = nullptr;
        }

        bool has_exited() const { return exited; }
        std::string name() const { return nm; }

        kthread() = delete;
        kthread(const kthread&) = delete;
        kthread &operator=(const kthread&) = delete;
    protected:
        std::string nm;
        bool stop_requested;
        bool exited;
        std::thread *pthrd;
};

}
