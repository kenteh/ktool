/**
 * @author Ken Teh <teh@anl.gov>
 */
#ifndef _sigpair_h
#define _sigpair_h  1
#include <signal.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

typedef void (*sigpair_handler_t)(int);

struct sigpair {
    int                 signr;  /* Signal number (see signal.h). */
    sigpair_handler_t   hndlr;  /* Handler function for signr. */
};

/**
 * @brief Installs signal handlers.
 *  @param sigpairs[in] Null-terminated array of struct sigpair's.
 *  @Return 0 if successful, errno on error.
 *
 *  Usage:
 *
 *      void myhndlr(int signr, siginfo_t *nf, void *uc)
 *      {
 *          // signal handling here...
 *      }
 *
 *      struct sigpair sp[] = { { SIGINT, myhndlr }, { 0, 0 } };
 *      sigpairinstall(sp);
 */
int sigpairinstall(const struct sigpair *sigpairs);

__END_DECLS
#endif
