/**
 * @file stringutils.h
 * @author Ken Teh
 * @date 20191201
 *
 * Simple string utilities.
 */
#ifndef _stringutils_h
#define _stringutils_h
#include <string>
#include <vector>

namespace ktool {
namespace stringutils {
    typedef std::vector<std::string> stringvector_type;

    /**
     * @brief Split a string.
     */
    stringvector_type split(const std::string& s,
            const std::string& sep=std::string(), unsigned int maxsplit=0);

    /**
     * @brief Join a stringvector into a string.
     */
    std::string join(const stringvector_type& sv, const std::string& sep);

    /**
     * @brief Strip characters from both ends of a string.
     * @param[in] s     input string
     * @param[in] chars set of characters to strip
     * @return stripped string
     *
     * The default 'chars' strips whitespace from both ends. However it is
     * also possible to strip a set of characters by using the regular
     * expression character set construction, eg, chars="[xyz]+".
     */
    std::string strip(const std::string& s, const char* chars="\\s+");

    /**
     * @brief Strip leading characters.
     * @param[in] s     input string
     * @param[in] chars set of characters to strip
     * @return stripped string
     */
    std::string lstrip(const std::string& s, const char* chars="\\s+");

    /**
     * @brief Strip trailing characters.
     * @param[in] s     input string
     * @param[in] chars set of characters to strip
     * @return stripped string
     */
    std::string rstrip(const std::string& s, const char* chars="\\s+");

    /** 
     * @brief Convert all characters to lowercase.
     */
    std::string lower(const std::string& s);

    /**
     * @brief Convert all characters to uppercase.
     */
    std::string upper(const std::string& s);

    /**
     * @brief Title case a string
     */
    std::string title(const std::string& s);

    /**
     * @brief Decode a (possibly) binary string into a printable one.
     *
     * Equivalent to python's bytes.decode().
     */
    std::string bytedecode(const std::string&);

} // namespace stringutils
} // namespace ktool

#endif
