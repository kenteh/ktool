#include <ktool/stringutils.h>
#include <regex>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <cctype>

namespace ktool {
namespace stringutils {
    std::string join(const stringvector_type& sv, const std::string& j)
    {
        if (sv.size() == 0) return std::string();
        std::string joined(sv[0]);
        for (stringvector_type::size_type i=1; i < sv.size(); ++i)
            joined.append(j).append(sv[i]);
        return joined;
    }

    std::string strip(const std::string& s, const char* chars_)
    {
        std::string chars(chars_);
        std::string patt("^");

        if (chars_ == nullptr or chars_[0] == '\0') chars = "\\s+";
        if (chars[0] == '\\' or chars[0] == '[')
            patt.append(chars).append("|").append(chars).append("$");
        else
            patt.append("[").append(chars).append("]+|[").append(chars)
                .append("]+$");

        std::regex rx(patt);
        return std::regex_replace(s, rx, "");
    }

    std::string lstrip(const std::string &s, const char *chars_)
    {
        std::string chars(chars_);
        std::string patt("^");

        if (chars_ == nullptr or chars_[0] == '\0') chars = "\\s+";
        if (chars[0] == '\\' or chars[0] =='[')
            patt.append(chars);
        else
            patt.append(1, '[').append(chars).append("]+");

        std::regex rx(patt);
        return std::regex_replace(s, rx, "");
    }

    std::string rstrip(const std::string &s, const char *chars_)
    {
        std::string chars(chars_);
        std::string patt;

        if (chars_ == nullptr or chars_[0] == '\0') chars = "\\s+";
        if (chars[0] == '\\' or chars[0] =='[')
            patt.assign(chars).append(1, '$');
        else
            patt.assign(1, '[').append(chars).append("]+$");

        std::regex rx(patt);
        return std::regex_replace(s, rx, "");
    }

    stringvector_type split(const std::string& s, const std::string& sep,
            unsigned int maxsplit)
    {
        std::regex sepre = sep.size() == 0 ? std::regex("\\s+") :
            std::regex(sep);
        std::sregex_token_iterator i(s.begin(), s.end(), sepre, -1);
        std::sregex_token_iterator j;
        stringvector_type strings;

        if (maxsplit == 0) {
            while (i != j) strings.push_back(*i++);
        }
        else {
            unsigned int idx = 0;
            while (i != j) {
                if (idx <= maxsplit)
                    strings.push_back(i->str());
                else
                    strings[maxsplit].append(sep).append(i->str());
                ++idx;
                ++i;
            }
        }

        return strings;
    }

    std::string lower(const std::string& s)
    {
        std::string t(s);
        std::transform(t.begin(), t.end(), t.begin(),
                [](unsigned char c) { return std::tolower(c); });
        return t;
    }

    std::string upper(const std::string& s)
    {
        std::string t(s);
        std::transform(t.begin(), t.end(), t.begin(),
                [](unsigned char c) { return std::toupper(c); });
        return t;
    }

    std::string title(const std::string& s)
    {
        std::string t(s);
        std::regex rx("\\S+");
        std::sregex_iterator i(s.begin(), s.end(), rx);
        std::sregex_iterator j;

        while (i != j) {
            std::string w;
            w = i->str();
            std::transform(w.begin(), w.end(), w.begin(),
                    [](unsigned char c) { return std::tolower(c); });
            w[0] = std::toupper(w[0]);
            t.replace(i->position(), i->length(), w);
            ++i;
        }

        return t;
    }

    std::string bytedecode(const std::string &s)
    {
        std::ostringstream sout;
        for (auto &c : s) {
            if (isspace(c) or !isprint(c))
                sout << "\\x" << std::setw(2) << std::setfill('0')
                    << std::hex << int(c);
            else
                sout << c;
        }
        return sout.str();
    }
}// namespace stringutils
}// namespace ktool
