#include <errno.h>
#include <ktool/sigpair.h>

int sigpairinstall(const struct sigpair *ss)
{
    struct sigaction sa;
    const struct sigpair *s;

    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    for (s = ss; s->signr != 0; ++s) sigaddset(&sa.sa_mask, s->signr);
    for (s = ss; s->signr != 0; ++s) {
        struct sigaction csa;
        struct sigaction *sap;

        sa.sa_handler = s->hndlr;
        if (s->signr == SIGCHLD) {
            csa = sa;
            csa.sa_flags |= SA_NOCLDSTOP;
            sap = &csa;
        }
        else {
            sap = &sa;
        }
        if (sigaction(s->signr, sap, 0) == -1) return errno;
    }
    return 0;
}
